# JournoLink Technical Test
My first Lavarel project and it's pretty raw. To run just go to 

homestead.test

To scrape an rss feed use the artisan command "artisan getRSS {url}" 
For example:

artisan getRSS https://metro.co.uk/feed/

Would download the metro feed. This will then be displayed in the browser at homestead.test in a simple table of headlines with clickable links. 