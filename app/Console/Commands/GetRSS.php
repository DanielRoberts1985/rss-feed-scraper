<?php

namespace App\Console\Commands;

use App\RSS;
use Illuminate\Console\Command;

class GetRSS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'getRSS
                            {url : The URL to grab}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retrieve RSS Feed from Site';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		
		$i = 0; 
		$url = $this->argument('url');
		$rss = simplexml_load_file($url); 

		foreach($rss->channel->item as $item) {
			RSS::firstOrCreate(array("url" => $item->link, "title" => $item->title));
		}
		
        //
    }
}
