<?php

namespace App\Http\Controllers;

use App\RSS;
use Illuminate\Http\Request;

class RSSController extends Controller
{
    public function index()
	{
		$rss = \App\RSS::all();

		return view('welcome', ['rss' => $rss]);
	}
}
