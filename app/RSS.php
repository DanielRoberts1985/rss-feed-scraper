<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RSS extends Model
{
    protected $table = "r_s_s";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'title', 'url'
    ];
}
