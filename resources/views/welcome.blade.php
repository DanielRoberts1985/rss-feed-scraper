<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #000000;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

			td {
				border: solid 2px lightgrey;
				border-spacing: 0px;
			}
			
			a {
				color: #000000;
				text-decoration: none;
			}

			a:hover 
			{
				 color:#000000; 
				 text-decoration:none; 
				 cursor:pointer;  
			}
			
            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: left;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #000000;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">
				<?php
				
				echo '<table>';
				
				?>
				
				@foreach($rss as $entry)
				
					<tr>
						<td>
							<a href= {{$entry->url}}> {{$entry->title}} </a><br />
						</td>
					</tr>
				
				@endforeach
				</table>
            </div>
        </div>
    </body>
</html>
